public class Alamat {
    private String prov;
    private String kota;
    private String kec;
    private String kel;

    // Constructor Alamat
    public Alamat(String prov, String kota, String kec, String kel) {
        this.prov = prov;
        this.kota = kota;
        this.kec = kec;
        this.kel = kel;
    }

    public String getProv() {
        return this.prov;
    }

    public String getKota() {
        return this.kota;
    }

    public String getKec() {
        return this.kec;
    }

    public String getKel() {
        return this.kel;
    }

    public String toString() {
        return ("Provinsi: " + getProv() + "\nKota: " + getKota() + "\nKec: " + getKec() + "\nKel: " + getKel());
    }

}