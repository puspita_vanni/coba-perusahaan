import java.util.*;

public class ZProgram {
    private static List<PerusahaanInduk> listInduk = new ArrayList<>();
    ZProgram data = new ZProgram();

    public static void main(String[] args) {

        HashMap<PerusahaanInduk, PerusahaanCabang[]> data = new HashMap<PerusahaanInduk, PerusahaanCabang[]>();

        Alamat a = new Alamat("Jakarta", "JakSel", "Sudirman", "kelurahan");
        Alamat b = new Alamat("Bandung", "JakSel", "Sudirman", "kelurahan");
        // System.out.println(a.toString());

        PerusahaanInduk satu = new PerusahaanInduk("ASTRA", a, "021345", "1234", "astra@gmail.com", 21);
        PerusahaanInduk dua = new PerusahaanInduk("GOJEK", b, "021345", "1256", "gojek@gmail.com", 22);
        // System.out.println(satu.toString());

        listInduk.add(satu);
        listInduk.add(dua);

        PerusahaanCabang[] listCabangAstra = new PerusahaanCabang[2];
        PerusahaanCabang[] listCabangGojek = new PerusahaanCabang[1];

        Alamat a_1 = new Alamat("Jakarta", "JakPus", "Menteng", "ini kelurahan");
        Alamat a_2 = new Alamat("Jawa Barat", "Depok", "Margonda", "kelurahan");
        Alamat b_1 = new Alamat("Jawa Timur", "Depok", "Margonda", "kelurahan");

        PerusahaanCabang cabangA = new PerusahaanCabang("anakASTRA_1", a_1, "02138885", "1234", "astra_1@gmail.com",
                satu);
        listCabangAstra[0] = cabangA;

        PerusahaanCabang cabangB = new PerusahaanCabang("anakASTRA_2", a_2, "021345", "1267", "astra_2@gmail.com",
                satu);
        listCabangAstra[1] = cabangB;

        PerusahaanCabang cabangC = new PerusahaanCabang("anakGOJEK_1", b_1, "2145", "562", "gojek_1@gmail.com", dua);
        listCabangGojek[0] = cabangC;

        data.put(satu, listCabangAstra);
        data.put(dua, listCabangGojek);

        for (PerusahaanInduk p : data.keySet()) {
            // System.out.println(p);
            // for (PerusahaanCabang[] c : data.values()) {
            System.out.println(p + "\n\n");
            int t = data.get(p).length;
            for (int i = 0; i < t; i++) {
                System.out.println(data.get(p)[i]);
            }
            System.out.println();
        }

    }

}