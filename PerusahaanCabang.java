import java.util.*;

public class PerusahaanCabang extends Perusahaan {

    private PerusahaanInduk induk;
    private List<PerusahaanCabang> listCabang = new ArrayList<>();

    public PerusahaanCabang(String nama, Alamat alamat, String telp, String fax, String email, PerusahaanInduk induk) {
        super(nama, alamat, telp, fax, email);
        this.induk = induk;
    }

    public PerusahaanInduk getInduk() {
        return this.induk;
    }

    public void addCabang(PerusahaanCabang c) {
        this.listCabang.add(c);
        // System.out.println(p + "typenya: " + p.get)
    }

    public String toString() {
        // String a = getInduk().getNama();
        String stringCabang = ("---DATA CABANG " + getNama() + "\nNama Cabang: " + getNama() + "\n" + "No telp: "
                + getTelp() + " \nFax: " + getFax() + " \nEmail: " + getEmail() + "\n\n---Alamat Cabang " + getNama()
                + "---" + "\n" + getAlamat() + "\n");
        return stringCabang;
    }
}