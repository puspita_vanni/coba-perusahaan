public class IzinUsahaPenangkaran extends IzinUsaha {
    private String bidang;

    public IzinUsahaPenangkaran(int no, String tanggal, String oleh, String bidang) {
        super(no, tanggal, oleh);
        this.bidang = bidang;
    }

    public String getBidang() {
        return bidang;
    }
}
