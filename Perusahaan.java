public class Perusahaan {
    private String nama;
    private Alamat alamat;
    private String telp;
    private String fax;
    private String email;

    public Perusahaan(String nama, Alamat alamat, String telp, String fax, String email) {
        this.nama = nama;
        this.alamat = alamat;
        this.telp = telp;
        this.fax = fax;
        this.email = email;
    }

    public String bentukUsaha(int n) {
        String bentuk;
        if (n == 1) {
            bentuk = "koperasi";
        } else if (n == 2) {
            bentuk = "firma";
        } else {
            bentuk = "PT";
        }
        return bentuk;
    }

    public String statusMilik(int n) {
        String milik;
        if (n == 1) {
            milik = "milikA";
        } else if (n == 2) {
            milik = "milikB";
        } else {
            milik = "milikC";
        }
        return milik;
    }

    public String statusPerusahaan(int n) {
        String prshn;
        if (n == 1) {
            prshn = "perusahaanA";
        } else if (n == 2) {
            prshn = "perusahaanB";
        } else {
            prshn = "perusahaanC";
        }
        return prshn;
    }

    public String getNama() {
        return this.nama;
    }

    public String getTelp() {
        return this.telp;
    }

    public String getFax() {
        return this.fax;
    }

    public String getEmail() {
        return this.email;
    }

    public Alamat getAlamat() {
        return this.alamat;
    }

}